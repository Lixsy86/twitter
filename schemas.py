from typing import Optional

from pydantic import BaseModel


class PersonsInfo(BaseModel):
    id: Optional[int]
    email: str
    phone_number: str
    name: str
    surname: str


class Users(BaseModel):
    id: Optional[int]
    username: str
    password: Optional[str]
    disabled: Optional[bool]


# class UsersPassword(BaseModel):
#     password: str


class Message(BaseModel):
    message_body: str


class MessageOnWall(BaseModel):
    id: Optional[int]
    body: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None

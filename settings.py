from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    use_pg: bool = False
    pg_user: Optional[str] = None
    pg_pass: Optional[str] = None
    pg_host: Optional[str] = None
    pg_port: str = "5432"
    pg_db: Optional[str] = None
    sqlite_name: str = "test_db.db"


env = Settings()

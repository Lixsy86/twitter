import hashlib

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
import sqlalchemy
import models
import schemas
from database import SessionLocal


def calculate_hash(data: str) -> str:
    data_hash = hashlib.sha256(data.encode()).hexdigest()
    return data_hash


def create_user(db: Session, user: schemas.Users) -> models.UserModel:
    hash_pass = calculate_hash(user.password)
    db_user = models.UserModel(username=user.username, password=hash_pass, disabled=False)
    db.add(db_user)
    db.commit()
    return db_user


def create_personal_info(db: Session, person: schemas.PersonsInfo) -> models.PersonsInfoModel:
    db_person = models.PersonsInfoModel(name=person.name, surname=person.surname, email=person.email,
                                        phone_number=person.phone_number)
    db.add(db_person)
    db.commit()
    return db_person


def create_admin():
    db = SessionLocal()
    first_user: schemas.Users = schemas.Users(id=1, username="admin", password="password", disabled=False)
    first_users_info: schemas.PersonsInfo = schemas.PersonsInfo(id=1, email="admin@gmail.com", phone_number=9277821312,
                                                                name="Manuel", surname="Krasinski")
    try:
        create_user(db, first_user)
        create_personal_info(db, first_users_info)
    except IntegrityError:
        pass
    db.close()


def admin_to_auth(db: Session, admin_id: int):
    admin: dict = db.query(models.UserModel).filter(models.UserModel.id == admin_id).first()
    admin_with_info = admin
    admin_with_info[admin]["name"] = models.PersonsInfoModel.name
    admin_with_info[admin]["surname"] = models.PersonsInfoModel.surname
    admin_with_info[admin]["email"] = models.PersonsInfoModel.email
    admin_with_info[admin]["phone_number"] = models.PersonsInfoModel.phone_number
    admin_with_info[admin]["password"] = models.PersonsInfoModel.password
    return admin_with_info[admin]


def get_one_user_by_id(db: Session, user_id: int):
    db_user = db.query(models.UserModel).filter(models.UserModel.id == user_id).first()
    return db_user


def get_one_user_by_nickname(db: Session, username: str):
    db_user = db.query(models.UserModel).filter(models.UserModel.username == username).first()
    return db_user


def get_personal_info(db: Session, user_id: int):
    personal_info = db.query(models.PersonsInfoModel).filter(models.UserModel.id == user_id).all()
    return personal_info


def authentication_user_and_password(db: Session, username: str, password: str) -> bool:
    result = db.query(models.UserModel).filter(models.UserModel.username == username).first()
    if result is None:
        return False
    return result.password == calculate_hash(password)


# def make_user_admin():
#     complete_user = {"first_name": "",
#                      "last_name": "",
#                      "email": "",
#                      "phone_number": "",
#                      "disabled": False}
#     print(" Your first name is ")
#     first_name = input()
#     complete_user["first_name"] = first_name
#     print(" Your last name is ")
#     last_name = input()
#     complete_user["last_name"] = last_name
#     print(" Your email address is: ")
#     email = input()
#     complete_user["email"] = email
#     print(" Your phone number is \n")
#     phone_number = input("+7")
#     while len(phone_number) != 10:
#         print("Please write your phone number correctly!")
#         phone_number = input("+7")
#     try:
#         phone_number = int(phone_number)
#         complete_user["phone_number"] = phone_number
#     except ValueError:
#         print("Please write your phone number correctly!")
#         phone_number = input("+7")


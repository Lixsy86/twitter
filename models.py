from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship


class UserModel(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)
    disabled = Column(Boolean)
    # persons_id = Column(Integer, ForeignKey("person.id"))
    # likes = relationship("LikeModel", back_populates="amount")



class PersonsInfoModel(Base):
    __tablename__ = "person"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    surname = Column(String, index=True)
    email = Column(String, unique=True)
    phone_number = Column(String, unique=True)
    # users_id = Column(Integer, ForeignKey("users.id"))


class WallMessageModel(Base):
    __tablename__ = "message_on_wall"

    id = Column(Integer, primary_key=True, index=True)
    body = Column(Integer)
    wall_id = Column(Integer, ForeignKey("users.id"))


class MessageModel(Base):
    __tablename__ = "message"

    id = Column(Integer, primary_key=True, index=True)
    body = Column(String)

#
# class LikeModel(Base):
#     __tablename__ = "likes"
#
#     id = Column(Integer, primary_key=True)
#     quantity = Column(Integer)
#     UserModel_id = Column(Integer, ForeignKey("users.id"))
#     amount = relationship("UserModel", back_populates="likes")
